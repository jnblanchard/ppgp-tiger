#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <vector>

class Pi_Estimator {
	private:
		int count;
		int iterations;
	
	public:
		double pi, error;
		Pi_Estimator(int iterations);
		void set_values();
		void report();
	};
	
	Pi_Estimator::Pi_Estimator(int iterations) {
		this->iterations = iterations;
		this->count = 0;
	}
	
	void Pi_Estimator::set_values() {
		double x, y, z;
		int seed = rand() % 100 + 35791146;
		srand(seed);
		for(int i = 0; i < this->iterations; i++) {
			x = (double)rand()/RAND_MAX;
			y = (double)rand()/RAND_MAX;
			z = x*x+y*y;
			if(z <= 1) {
				this->count++;
			}
		}
		this->pi = (double)count/iterations*4;
//		if(this->pi > 4 || this->pi < 3) {
//			this->set_values();
//		}
		this->error = ((pi - M_PI)/M_PI);
		if(((pi - M_PI)/M_PI) < 0) {
			this->error = -1*((pi - M_PI)/M_PI);
		}
	}
	
	void Pi_Estimator::report() {
		printf("Number of trials = %d \nEstimate of pi is %g \n",this->iterations,this->pi);
		printf("Error: %g \n", this->error);
	}

std::vector<Pi_Estimator*> estimations;
std::vector<Pi_Estimator*> estimationsTwo;
std::vector<Pi_Estimator*> estimationsThree;

int main ( int argc, char* argv[] ) {
	double error, pi, errorTwo, piTwo, errorThree, piThree;
	double meanError = 0, meanPi = 0, meanErrorTwo = 0, meanPiTwo = 0, meanErrorThree = 0, meanPiThree = 0;
	int num_pi = 20;
	for(int i = 0; i < num_pi; i++) {
		Pi_Estimator* estimator = new Pi_Estimator(10000 * (i+1));
		Pi_Estimator* estimatorTwo = new Pi_Estimator(100000 * (i+1));
		Pi_Estimator* estimatorThree = new Pi_Estimator(1000000 * (i+1));
		estimator->set_values();
		estimatorTwo->set_values();
		estimatorThree->set_values();
		estimator->report();
		estimatorTwo->report();
		estimatorThree->report();
		estimations.push_back(estimator);
		estimationsTwo.push_back(estimatorTwo);
		estimationsThree.push_back(estimatorThree);
		delete estimator;
		delete estimatorTwo;
		delete estimatorThree;
	}
	for(int i = 0; i < num_pi; i++) {
		pi += estimations[i]->pi;
		piTwo += estimationsTwo[i]->pi;
		piThree += estimationsThree[i]->pi;
		error += estimations[i]->error;
		errorTwo += estimationsTwo[i]->error;
		errorThree += estimationsThree[i]->error;
	}
	printf("--------------------------\n");
	meanPi = pi / (double)num_pi;
	meanPiTwo = piTwo / (double)num_pi;
	meanPiThree = piThree / (double)num_pi;
	meanError = error / (double)num_pi;
	meanErrorTwo = errorTwo / (double)num_pi;
	meanErrorThree = errorThree / (double)num_pi;
	printf("Mean Pi: %g \nMean Error: %g \n", meanPi, meanError);
	printf("Mean Pi Two: %g \nMean Error Two: %g \n", meanPiTwo, meanErrorTwo);
	printf("Mean Pi Three: %g \nMean Error Three: %g \n", meanPiThree, meanErrorThree);
	
}
		
