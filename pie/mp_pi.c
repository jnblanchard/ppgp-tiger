#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <omp.h>

class Pi_Estimator {
	private:
		int iterations;
	public:
		double pi, error;
		Pi_Estimator(int iterations);
		double get_rand();
		void set_values();
		void report();
	};
	
	Pi_Estimator::Pi_Estimator(int iterations) {
		this->iterations = iterations;
	}
	
	double Pi_Estimator::get_rand() {
		double dub = (double)rand()/RAND_MAX;
		return dub*dub;
	}
	
	void Pi_Estimator::set_values() {
		double x, y, z;
		int count = 0, i;
		int numThreads = 8;
		#pragma omp parallel firstprivate(x, y, z, i) shared(count) num_threads(numThreads)
		{
			int seed = rand() % 100 + 35791146;
			srand(seed);
			for(i = 0; i < this->iterations; i++) {
				x = get_rand();
				y = get_rand();
				z = sqrt(x+y);
				if(z <= 1) {
					count++;
				}
			}
		}
		this->pi = (double)count/iterations*4;
		this->error = ((pi - M_PI)/M_PI);
		if(((pi - M_PI)/M_PI) < 0) {
			this->error = -1*((pi - M_PI)/M_PI);
		}
	}
	
	void Pi_Estimator::report() {
		printf("Number of trials = %d \nEstimate of pi is %g \n",this->iterations,this->pi);
		printf("Error: %g \n", this->error);
	}

int num_pi = 1;

int main ( int argc, char* argv[] ) {
	double error, pi;
	double meanError = 0, meanPi = 0; 
	Pi_Estimator* estimator = new Pi_Estimator(1000000);
	estimator->set_values();
	estimator->report();
	pi = estimator->pi;
	error = estimator->error;
	printf("--------------------------\n");
	meanPi = pi / (double)num_pi;
	meanError = error / (double)num_pi;
	printf("Mean Pi: %g \nMean Error: %g \n", meanPi, meanError);
}
		
