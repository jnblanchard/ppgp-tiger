/*
 This program implements the BBP algorithm to generate a few hexadecimal
 digits beginning immediately after a given position id, or in other words
 beginning at position id + 1.  On most systems using IEEE 64-bit floating-
 point arithmetic, this code works correctly so long as d is less than
 approximately 1.18 x 10^7.  If 80-bit arithmetic can be employed, this limit
 is significantly higher.  Whatever arithmetic is used, results for a given
 position id can be checked by repeating with id-1 or id+1, and verifying
 that the hex digits perfectly overlap with an offset of one, except possibly
 for a few trailing digits.  The resulting fractions are typically accurate
 to at least 11 decimal digits, and to at least 9 hex digits.
 
 */

/*  David H. Bailey     2006-09-08 */

// Reference http://www.experimentalmath.info/bbp-codes/bbp-alg.pdf
// http://www.divms.uiowa.edu/help/seminar/pi/piqpr8.c

#include <stdio.h>
#include <math.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <cilk/cilk.h>

#define USE_LONG_DOUBLE 1

// Per David Bailey the length of the hex string calcualted depends upon the
// the szie of the floating point reprsentation.
//
// "Using IEEE 64-bit floating-point arithmetic (available on almost all computers)
//  typically yields 9 or more correct hex digits (for reasonable-sized d); using
//  80-bit floating-point arithmetic (available on Intel and AMD processors) typically
//  yields 10 or more correct hex digits; using 128-bit arithmetic (available on some
// systems at least in software) typically yields 24 or more correct hex digits."
//
#if USE_LONG_DOUBLE
    // 128 bits on OS X
    typedef long double BigFloat;
    typedef long long PosID;
    #define BigFloatFormat "%.15Lf"
    #define PosIDFormat "%lld"
    #define NUM_HEX_DIGITS 11           // Bailey says we can get 25 hex digits here but empericallly we get less - especially for large hex positions.
    #define BFabs(x) fabsl(x)
    #define BFfloor(x) floorl(x)
    #define BFpow(x, y) powl(x, y)
#else
    // 64 bits on OS X
    typedef double BigFloat;
    typedef int PosID;
    #define BigFloatFormat "%.15f"
    #define PosIDFormat "%d"
    #define NUM_HEX_DIGITS 9
    #define BFabs(x) fabs(x)
    #define BFfloor(x) floor(x)
    #define BFpow(x, y) pow(x, y)
#endif


typedef struct {
    PosID start;
    PosID end;
} PosRange;


/* Prototypes */

void initTPTable(void);
BigFloat series (unsigned int m, PosID n);
void ihex (BigFloat x, int m, char c[]);
void generatePiHexDigits(PosID position, char *hexBuffer);
void generatePiHexBlock(PosID position, char *hexBuffer, size_t hexBufferSize);
BigFloat expm (BigFloat p, BigFloat ak);
void splitUpWork(PosID start, PosID end);
void splitSegments(const PosRange *inSegments, int numInSegments, PosRange *outSegments);
PosID calcPartition(PosID start, PosID end);

/* Globals */

//For the threads, a read-only table of powers of 2.
#define ntp 50
static BigFloat tp[ntp];

/* Code */

int main()
{
    initTPTable();
    
    splitUpWork(0, 1e4);

    printf ("Floating point size = %d bits.\n", (int) sizeof(BigFloat) * 8);
    
}



// Number of threads needs to be power of 2 for the partitioning
#define NumberOfSplits 3
#define NumberOfThreads (1 << NumberOfSplits)

void splitUpWork(PosID start, PosID end)
{
    PosRange segments1[NumberOfThreads];
    PosRange segments2[NumberOfThreads];
    PosRange *inSegments = segments1;
    PosRange *outSegments = segments2;
    
    inSegments[0].start = start;
    inSegments[0].end = end;
    
    for (int i = 0; i < NumberOfSplits; i++) {
        splitSegments(inSegments, 1 << i, outSegments);
        PosRange *tmp = outSegments;
        outSegments = inSegments;
        inSegments = tmp;
    }
    
    // inSegments holds the ranges we want to work on with our threads
    

    // A buffer to hold all of the digits. It needs to be a little larger in
    // case we are creating more digits than needed.
    char *hexDigits = malloc((size_t)(end - start + NUM_HEX_DIGITS));


    // start the threads here using the range
    cilk_for (int i = 0; i < NumberOfThreads; i++) {
        printf("Segment %d: start:" PosIDFormat " end:" PosIDFormat " size:" PosIDFormat "\n", i, inSegments[i].start, inSegments[i].end, inSegments[i].end - inSegments[i].start);
        PosID digitsInRange = inSegments[i].end - inSegments[i].start;
        generatePiHexBlock(inSegments[i].start, &hexDigits[inSegments[i].start], digitsInRange);
    }
    
    
    for (size_t i = 0; i < (size_t)(end - start); i++) {
        fputc(hexDigits[i], stdout);
    }
    
    fputc('\n', stdout);

    
}


void splitSegments(const PosRange *inSegments, int numInSegments, PosRange *outSegments)
{
    for (int i = 0; i < numInSegments; i++) {
        PosRange range = inSegments[i];
        PosID partition = calcPartition(range.start, range.end);
        outSegments[i * 2].start = inSegments[i].start;
        outSegments[i * 2].end = partition;
        outSegments[i * 2 + 1].start = partition;
        outSegments[i * 2 + 1].end = inSegments[i].end;
    }

}

PosID calcPartition(PosID start, PosID end)
{
	BigFloat sum = (end * end + start * start) / 2;
	return (PosID) sqrtl(sum);
}

// 'hexBuffer' must be able to hold NUM_HEX_DIGITS bytes
void generatePiHexDigits(PosID position, char *hexBuffer)
{
    BigFloat s1 = series (1, position);
    BigFloat s2 = series (4, position);
    BigFloat s3 = series (5, position);
    BigFloat s4 = series (6, position);
    
    BigFloat pid = (BigFloat)4. * s1 - (BigFloat)2. * s2 - s3 - s4;
    pid = pid - (PosID) pid + 1.;
    ihex (pid, NUM_HEX_DIGITS, hexBuffer);
    
}

// 'hexBufferSize' must be a multiple of NUM_HEX_DIGITS
void generatePiHexBlock(PosID position, char *hexBuffer, size_t hexBufferSize)
{
    char *inHexBuffer = hexBuffer;
    
    PosID tooFarPosition = (PosID)(position + hexBufferSize);
    cilk_for(PosID currentPosition = position; currentPosition < tooFarPosition; currentPosition += NUM_HEX_DIGITS) {
        generatePiHexDigits(currentPosition, inHexBuffer);
        inHexBuffer += NUM_HEX_DIGITS;
    }
    
}

void ihex (BigFloat x, int nhx, char chx[])

/*  This returns, in chx, the first nhx hex digits of the fraction of x. */

{
    int i;
    BigFloat y;
    char hx[] = "0123456789ABCDEF";
    
    y = BFabs (x);
    
    for (i = 0; i < nhx; i++){
        y = 16. * (y - BFfloor(y));
        //printf(BigFloatFormat"\n", y);
        chx[i] = hx[(int) y];
        
        //printf("%c", chx[i]);
    }
}

BigFloat series (unsigned int m, PosID id)

/*  This routine evaluates the series  sum_k 16^(id-k)/(8*k+m)
 using the modular exponentiation technique. */

{
    PosID k;
    BigFloat ak, p, s, t;
    BigFloat expm (BigFloat x, BigFloat y);
//#define eps 1e-17 // For double operation 
#define eps 1e-200L // For long double operation
  
    s = 0.;
    
    /*  Sum the series up to id. */
    
    for (k = 0; k < id; k++){
        ak = (PosID)8 * k + m;
        p = id - k;
        t = expm (p, ak);
        s = s + t / ak;
        s = s - (PosID) s;
    }
    
    /*  Compute a few terms where k >= id. */
    
    for (k = id; k <= id + 100; k++){
        ak = 8 * k + m;
        t = BFpow(16., (BigFloat) (id - k)) / ak;
        if (t < eps) break;
        s = s + t;
        s = s - (PosID) s;
    }
    return s;
}

void initTPTable()
{
    int i;
    tp[0] = 1.;
    for (i = 1; i < ntp; i++) tp[i] = 2. * tp[i-1];
}

BigFloat expm (BigFloat p, BigFloat ak)

/*  expm = 16^p mod ak.  This routine uses the left-to-right binary
 exponentiation scheme. */

{
    int i, j;
    BigFloat p1, pt, r;

    
    if (ak == 1.) return 0.;
    
    /*  Find the greatest power of two less than or equal to p. */
    
    for (i = 0; i < ntp; i++) if (tp[i] > p) break;

    if (ntp - i < 2) {
        printf("WARNING - hitting top end of power of 2 table. p=" BigFloatFormat "\n", p);
    }

    pt = tp[i-1];
    p1 = p;
    r = 1.;
    
    /*  Perform binary exponentiation algorithm modulo ak. */
    
    for (j = 1; j <= i; j++){
        if (p1 >= pt){
            r = 16. * r;
            r = r - (int) (r / ak) * ak;
            p1 = p1 - pt;
        }
        pt = 0.5 * pt;
        if (pt >= 1.){
            r = r * r;
            r = r - (int) (r / ak) * ak;
        }
    }
    
    return r;
}
